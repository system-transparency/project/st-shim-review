# System Transparency Boot Process Overview

System Transparency is an observability and transparency project which enables verification & attestation primitives for data center environments. It helps you to enable [confidential computing](https://confidentialcomputing.io/) in your data center and keeping control and transparency of all software components. The System Transparency project can be described as a toolbox to establish trust and easy maintenance in your data center.

## Overview

![](./st_boot_process.png)

### UEFI
The UEFI firmware offers to load nowadays a UEFI Secure Boot ready EFI application by http/https netboot. This feature is included for [quite a while](https://uefi.org/sites/default/files/resources/FINAL%20Pres4%20UEFI%20HTTP%20Boot.pdf). The UEFI firmware needs to be pre/runtime configured and networking stack with DHCPv4/v6 needs to be enabled on internal virtual network.

### SHIM
The SHIM we built here comes with the following options enabled:

SHIM Configuration
```
VENDOR_CERT_FILE=glasklar.der
DISABLE_EBS_PROTECTION=true
DISABLE_REMOVABLE_LOAD_OPTIONS=true
DEFAULT_LOADER="\\\\st.efi"
```

**Example SBAT Entries**
```
sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
shim,3,UEFI shim,shim,1,https://github.com/rhboot/shim
shim.glasklar,3,Glasklar Teknik AB,shim,15.7,https://www.glasklarteknik.se/
```
Since we are loading a custom Linux kernel based bootloader and want to restrict execution to ST only.

#### Example Netboot DHCP Configuration
```
default-lease-time 14400;
ddns-update-style none;
  subnet 192.168.100.0 netmask 255.255.255.0 {
    authoritative;
    range dynamic-bootp 192.168.100.101 192.168.100.253;
    default-lease-time 14400;
    max-lease-time 172800;
    option subnet-mask 255.255.255.0;
    option broadcast-address 192.168.100.255;
    option domain-name-servers 8.8.8.8;
    option routers 192.168.100.2;
    class "pxeclients"{
      match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
      next-server 192.168.100.2;
      filename "/st.efi";
    }
    class "httpclients" {
      match if substring (option vendor-class-identifier, 0, 10) = "HTTPClient";
      option vendor-class-identifier "HTTPClient";
      filename "http://192.168.100.2/st.efi";
    }
}
```

### STBOOT
![](./stboot_process.png)

The stboot bootloader consists of a standard Linux [kernel](https://git.glasklar.is/system-transparency/core/system-transparency/-/blob/main/contrib/default.config#L43) and a custom bootloader based on [u-root](u-root.org) as [initramfs](https://git.glasklar.is/system-transparency/core/stmgr). These artifacts will be packaged into an [Unified Kernel Image](https://github.com/uapi-group/specifications/blob/main/specs/unified_kernel_image.md) (uki), specified by the UAPI group.


We use internal tooling called [stmgr](https://git.glasklar.is/system-transparency/core/stmgr) to sign the stboot binary. We use here the standard PE signing mechanism used in [PE/COFF](https://learn.microsoft.com/de-de/windows/win32/debug/pe-format?redirectedfrom=MSDN) format.

**Example sbat.csv**
```
sbat,1,SBAT Version,sbat,1,https://github.com/rhboot/shim/blob/main/SBAT.md
stboot,1,Glasklar Teknik AB,stboot,1.0.0,https://git.glasklar.is/system-transparency/core/stboot
```

**Create signed stboot EFI**
```
stmgr uki create -format uki -kernel vmlinux -initrd initramfs -sbat sbat.csv -out stboot.efi
```

### OS Package
The OS package is zip file based package containing boot instructions via kexec to launch a designated operating system. Information about it can be found [here](https://git.glasklar.is/system-transparency/core/system-transparency#os-package).

**Signature Verfication**
Some information about the [signature verifcation of OS packages](https://git.glasklar.is/system-transparency/core/system-transparency#signature-verification).


