FROM debian:bullseye

RUN echo "deb-src http://deb.debian.org/debian bullseye main" >> /etc/apt/sources.list
RUN echo "deb-src http://deb.debian.org/debian-security bullseye-security main" >> /etc/apt/sources.list
RUN apt-get update -y
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends build-essential git-buildpackage dos2unix

#COPY *.efi /shim-review/

WORKDIR /build
RUN git clone --recursive -b 15.7 https://github.com/rhboot/shim.git

COPY glasklar.cer /build
COPY sbat.csv /build/shim/data

# Build for x86_64 arch
RUN make -C shim ARCH=x86_64 VENDOR_CERT_FILE=/build/glasklar.cer DISABLE_REMOVABLE_LOAD_OPTIONS=true DEFAULT_LOADER="\\\\st.efi"

RUN ls /build/shim

# Enable NX compatibility flag as required by https://github.com/rhboot/shim-review/issues/307
RUN /build/shim/post-process-pe -vv -n /build/shim/shimx64.efi

# Copy binary targets
RUN mkdir /build/target/
RUN cp /build/shim/shimx64.efi /build/target

# Check if SBAT is compiled into targets correctly
RUN objcopy -j .sbat -O binary /build/target/shimx64.efi shimx64.sbat
RUN cat shimx64.sbat 
RUN diff -u shimx64.sbat /build/shim/data/sbat.csv || ( >&2 echo "Bad SBAT"; exit 1 )

# Check for original and built SHA256
RUN sha256sum /build/target/shimx64.efi /shim-review/shimx64.efi

RUN hexdump -Cv /build/target/shimx64.efi > shimx64-built-dump && \
    hexdump -Cv /shim-review/shimx64.efi > shimx64-orig-dump && \
RUN diff -u shimx64-built-dump shimx64-orig-dump || ( >&2 echo "Target binaries are not the same as original"; exit 1 )